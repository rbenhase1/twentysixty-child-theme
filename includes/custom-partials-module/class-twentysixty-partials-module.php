<?php
/*
Author: 2060 Digital
URL: http://2060digital.com
*/


/**
 * Helper class for custom partials.
 *
 * @class TwentysixtyPartialsModule
 */
class TwentysixtyPartialsModule extends FLBuilderModule {

  public function __construct() {

      parent::__construct( array(
          'name'            => __( 'Custom Partial', 'twentysixty-theme' ),
          'description'     => __( 'A PHP/HTML snippet from the theme\'s "partials" folder', 'twentysixty-theme' ),
          'category'        => __( 'Advanced Modules', 'twentysixty-theme' ),
          'dir'             => get_stylesheet_directory() . '/includes/custom-partials-module/',
          'url'             => get_stylesheet_directory_uri() . '/includes/custom-partials-module/',
          'editor_export'   => true, // Defaults to true and can be omitted.
          'enabled'         => true, // Defaults to true and can be omitted.
          'partial_refresh' => false, // Defaults to false and can be omitted.
      ));
      
      
  }
}