/**
 * This file adds some LIVE to the Theme Customizer live preview. 
 */
( function( $ ) {
  
  function twentysixtyCustomize( section ) {
    wp.customize( section, function( value ) {
        console.log( 'B: ' + section);
    		value.bind( function( newval ) {
      		$.post( TwentysixtyTheme.ajaxurl, { action: 'get_partial', slug: newval }, function( response ) {        
            $( "#" + section ).html( response );       
          });
      		
    		} );
    	} );  
  }
  
  for ( var section in TwentysixtyTheme.sections ) {
    twentysixtyCustomize(section);        	
  } 
		
} )( jQuery );