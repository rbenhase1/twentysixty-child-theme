<?php
/*
Author: 2060 Digital
URL: http://2060digital.com
*/

/**
 * Helper class for theme functions.
 *
 * @class TwentysixtyTheme
 */
final class TwentysixtyTheme {
      
	/**
	 * The current version of the theme.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the theme.
	 */
	protected $version;
	
	/**
	 * The child theme directory path.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $theme_dir    The child theme directory path.
	 */
	protected $theme_dir;
	
	/**
	 * The child theme base URL.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $theme_url    The child theme base URL.
	 */
	protected $theme_url;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the Dashboard and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->version = '1.0.0';
		$this->load_hooks();
		
		$this->theme_dir = get_stylesheet_directory();
		$this->theme_url = get_stylesheet_directory_uri();
		
		include_once( $this->theme_dir . '/includes/class-twentysixty-partials.php' );
		$this->partials = new TwentysixtyPartials();
		
		include_once( $this->theme_dir . '/includes/custom-partials-module/class-twentysixty-partials-module.php' );
		$this->partialsModule = new TwentysixtyPartialsModule( $this->partials );
				
		// Load custom functions
		include_once( $this->theme_dir . '/functions-custom.php' );
	}
	
	
	/**
	 * Add all of our actions and filters.
	 * 
	 * @access private
	 * @return void
	 */
	private function load_hooks() {
  	
  	// Set option defaults (branding, etc.)
  	add_action( 'after_switch_theme', array( $this, 'set_defaults' ) );
  	
  	// Load stylesheet
  	add_action( 'fl_head', array( $this, 'stylesheet' ) );
  	
  	// Disable default dashboard widgets
    add_action( 'wp_dashboard_setup', array( $this, 'disable_default_dashboard_widgets' ) );
    
    // Customize login page
    add_action( 'login_enqueue_scripts', array( $this, 'login_css' ), 10 );
    add_filter( 'login_headerurl', array( $this, 'login_url' ) );
    add_filter( 'login_headertitle', array( $this, 'login_title' ) );
    
    // Customize admin footer text
    add_filter( 'admin_footer_text', array( $this, 'custom_admin_footer' ) );
    
    // Clean up head
    add_action( 'init', array( $this, 'head_cleanup' ) );
    
    // Remove WP version from RSS
    add_filter( 'the_generator', array( $this, 'rss_version' ) );
    
    // Remove injected css for recent comments widget
    add_filter( 'wp_head', array( $this, 'remove_wp_widget_recent_comments_style' ), 1 );
    
    // Clean up comment styles in the head
    add_action( 'wp_head', array( $this, 'remove_recent_comments_style' ), 1 );
    
    // Enqueue live preview script
    add_action( 'customize_preview_init', array( $this, 'customizer_live_preview' ) );
	}
	
	
	/**
	 * Set default values in wp_options. Called after theme switch.
	 * 
	 * @access public
	 * @return void
	 */
	public function set_defaults() { 	
  	  	
  	$theme_branding_options = array (
      'name' => '2060 Digital Base',
      'description' => 'A base theme for use on 2060 Digital Sites',
      'company_name' => '2060 Digital',
      'company_url' => 'http://2060digital.com',
      'screenshot_url' => $this->theme_url . '/screenshot.png',
    );
  	
  	// Theme branding
  	update_option( '_fl_builder_theme_branding', $theme_branding_options );
  	
  	// Page builder branding title
  	update_option( '_fl_builder_branding', '2060 Digital Page Builder' );
  	
  	// Page builder branding image
  	update_option( '_fl_builder_branding_icon', $this->theme_url . '/2060logo.png' );
  	
  	// Disable "WP Engine has your back" widget
  	update_option( 'wpengine_news_feed_enabled', 0 );
	}
	

  /**
   * Load stylesheet AFTER wp_head().
   * This is used (instead of wp_enqueue_style()) so that
   * the page builder overrides anything else. 
   * 
   * @access public
   * @return void
   */
  public function stylesheet() {
    echo '<link rel="stylesheet" href="' . $this->theme_url . '/library/css/style.css" />';
  }
  

  /**
   * Disable some built-in dashboard widgets.
   * 
   * @access public
   * @return void
   */
  public function disable_default_dashboard_widgets() {
  	global $wp_meta_boxes;
  	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);    // Right Now Widget
  	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);        // Activity Widget
  	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
  	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
  	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);         // Plugins Widget
  
  	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);    // Quick Press Widget
  	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);     // Recent Drafts Widget
  	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);           //
  	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);         //
  
  	// remove plugin dashboard boxes
  	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);           // Yoast's SEO Plugin Widget
  	unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);        // Gravity Forms Plugin Widget
  	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);   // bbPress Plugin Widget
  
  }
  
  
  /**
   * Custom login page styles.
   * 
   * @access public
   * @return void
   */
  public function login_css() {
	  wp_enqueue_style( 'login_css', $this->theme_url . '/library/css/login.css', false );
  }

  /*
   * Change Wordpress logo Link on login page
   * @access public
   * @return void
   */
  public function login_url() {  return home_url(); }
  
  /*
   * Change Wordpress alt attribute to blog name on login  page
   * @access public
   * @return void
   */ 
  public function login_title() { return get_option( 'blogname' ); }
  

  /**
   * Customize admin footer.
   * 
   * @access public
   * @return void
   */
  public function custom_admin_footer() {
  	_e( '<span id="footer-thankyou">Developed by <a href="http://2060digital.com" target="_blank">2060 Digital</a></span>.', 'twentysixtytheme' );
  }
  
  
  /**
   * Cleanup <head>.
   * 
   * @access public
   * @return void
   */
  public function head_cleanup() {
  	// category feeds
  	remove_action( 'wp_head', 'feed_links_extra', 3 );
  	// post and comment feeds
  	remove_action( 'wp_head', 'feed_links', 2 );
  	// EditURI link
  	remove_action( 'wp_head', 'rsd_link' );
  	// windows live writer
  	remove_action( 'wp_head', 'wlwmanifest_link' );
  	// previous link
  	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
  	// start link
  	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
  	// links for adjacent posts
  	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
  	// WP version
  	remove_action( 'wp_head', 'wp_generator' );
  	// remove WP version from css
  	add_filter( 'style_loader_src', array( $this, 'remove_wp_ver_css_js' ), 9999 );
  	// remove Wp version from scripts
  	add_filter( 'script_loader_src', array( $this, 'remove_wp_ver_css_js' ), 9999 );
  
  } /* end twentysixty head cleanup */
  

  /**
   * Remove WP Version from RSS.
   * 
   * @access public
   * @return String An empty string
   */
  public function rss_version() { return ''; }
  
 
  /**
   * remove WP version from scripts.
   * 
   * @access public
   * @param mixed $src
   * @return String $src The filtered src
   */
  public function remove_wp_ver_css_js( $src ) {
  	if ( strpos( $src, 'ver=' ) )
  		$src = remove_query_arg( 'ver', $src );
  	return $src;
  }
  
  /**
   * Remove injected CSS for recent comments widget
   * 
   * @access public
   * @return void
   */
  public function remove_wp_widget_recent_comments_style() {
  	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
  		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
  	}
  }
  

  /**
   * Remove injected CSS from recent comments widget.
   * 
   * @access public
   * @return void
   */
  public function remove_recent_comments_style() {
  	global $wp_widget_factory;
  	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
  		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
  	}
  }
  
  /**
   * Enqueue our theme customizer script.
   * Used by hook: 'customize_preview_init'
   * 
   * @see add_action('customize_preview_init',$func)
   */
  public function customizer_live_preview() {
    
  	wp_register_script( 
  		  'twentysixty-themecustomizer',	
  		  $this->theme_url . '/includes/min/customizer.js',
  		  array( 'jquery','customize-preview' ),	
  		  $this->version,						
  		  true					
  	);
  	
  	wp_localize_script( 
  	  'twentysixty-themecustomizer', 
  	  'TwentysixtyTheme', 
      array( 
	      'ajaxurl' => admin_url( 'admin-ajax.php' ),
	      'sections' => $this->partials->sections 
  	  ) 
    );
  	
  	wp_enqueue_script( 'twentysixty-themecustomizer' );
  }
  
}