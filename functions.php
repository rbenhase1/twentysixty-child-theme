<?php
/*
Author: 2060 Digital
URL: http://2060digital.com

This file loads everything needed for the child theme.
It is not recommended to make changes or customizations to this file.
Instead, make those in functions-custom.php (which is included).
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Disable WP Plugin & Theme Editor for Better Security
define( 'DISALLOW_FILE_EDIT', true );

// Get core theme class instance
require_once 'includes/class-twentysixty-theme.php';

/**
 * Launch the theme.
 */
$theme = new TwentysixtyTheme();