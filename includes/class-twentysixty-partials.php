<?php
/*
Author: 2060 Digital
URL: http://2060digital.com
*/

/**
 * Helper class for custom partials.
 *
 * @class TwentysixtyPartials
 */
final class TwentysixtyPartials {
  
  /**
	 * The custom partials to include
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Array    $partials    All of the partials to include
	 */
	public $partials;
	
  /**
	 * The custom sections for the Theme Customizer.
	 * Each corresponds to a BB theme hook. 
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Array    $sections The sections to include
	 */
	public $sections;
	
	/**
	 * Define the core functionality of the plugin.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		
		$this->partials = $this->get_partials();
		$this->sections = array(
      "twentysixty-body-open" => "After Body Open",
      "twentysixty-page-open" => "After Page Open",
      "twentysixty-before-top-bar" => "Before Top Bar",
      "twentysixty-after-top-bar" => "After Top Bar",
      "twentysixty-before-header" => "Before Header",
      "twentysixty-header-content-open" => "After Header Content Open",
      "twentysixty-header-content-close" => "Before Header Content Close",
      "twentysixty-after-header" => "After Header",
      "twentysixty-before-content" => "Before Main Content",
      "twentysixty-content-open" => "After Main Content Open",
      "twentysixty-post-top-meta-open" => "After Top Post Meta Open",
      "twentysixty-post-top-meta-close" => "Before Top Post Meta Close",
      "twentysixty-post-bottom-meta-open" => "After Bottom Post Meta Open",
      "twentysixty-post-bottom-meta-close" => "Before Bottom Post Meta Close",
      "twentysixty-comments-open" => "After Comments Open",
      "twentysixty-comments-close" => "Before Comments Close",
      "twentysixty-sidebar-open" => "After Sidebar Open",
      "twentysixty-sidebar-close" => "Before Sidebar Close", 
      "twentysixty-content-close" => "Before Main Content Close",
      "twentysixty-after-content" => "After Content",    
      "twentysixty-footer-wrap-open" => "After Footer Wrap Open",
      "twentysixty-before-footer-widgets" => "Before Footer Widgets",
      "twentysixty-after-footer-widgets" => "After Footer Widgets",
      "twentysixty-before-footer" => "Before Footer Bottom",
      "twentysixty-after-footer" => "After Footer Bottom",
      "twentysixty-footer-wrap-close" => "Before Footer Wrap Close",
      "twentysixty-page-close" => "Before Page Close",
      "twentysixty-body-close" => "Before Body Close"
    );
    
    require_once( get_template_directory() . '/classes/class-fl-customizer.php' );
    $this->settings = FLCustomizer::get_mods();
    
    $this->load_hooks();
		$this->add_to_customizer();
	}
	
	
	/**
	 * Add all of our actions and filters.
	 * 
	 * @access private
	 * @return void
	 */
	private function load_hooks() {
  	
    add_shortcode( 'custom_partial', array( $this, 'render_partial_shortcode' ) );
    add_action( 'wp_ajax_get_partial', array( $this, 'get_partial_ajax' ) );
    add_action( 'init', array( $this, 'load_module' ) );
    
    
    foreach( array_keys( $this->sections ) as $section ) {

      $hook = str_replace( 
        array( 'twentysixty', '-' ),
        array( 'fl', '_' ),
        $section
      );

      $action = function() use( $section ) { 
        
        echo '<div class="twentysixty-partial" id="' . $section . '">';
        if ( isset( $this->settings[$section] ) && $this->settings[$section] !== 'none' )
          $this->render_partial( $this->settings[$section] ); 
        echo '</div><!-- end #' . $section . ' -->';
      }; 
      
      add_action( 
        $hook, 
        $action
      );
        
      
    }
    

	}
	
	
  /**
   * Read custom partials and load into customizer interface.
   * 
   * @access public
   * @return Array $partials A list of partials to include  
   */
  private function get_partials() {
          
      $partials = array(
      	'none'          => __('None', 'twentysixty-child')	
      );
      
      /* Get partial files */
      if ( $handle = opendir( get_stylesheet_directory() . '/partials' ) ) {
        while ( false !== ( $file = readdir( $handle ) ) ) {
          if ( $file != "." && $file != ".." && strtolower( substr( $file, strrpos ( $file, '.' ) + 1 ) ) == 'php' ) {      
            $slug = str_replace( '.php', '', $file );      
            // Attempt to get human readable name from partial file, add to list      
            $data = get_file_data( get_stylesheet_directory() . '/partials/' . $file, array( 'PartialName' => 'Partial Name' ) );              
            $partials[$slug] = ( isset( $data['PartialName'] ) ? $data['PartialName'] : $slug );
          }
        }
        closedir($handle);
      }
    return $partials;
  }   
  
  /**
   * Render a single partial, given its slug.
   * 
   * @access public
   * @param String $slug The partial slug
   * @return void
   */
  public function render_partial( $slug ) {
    if ( $slug !== 'none' )
      include( get_stylesheet_directory() . '/partials/' . $slug . '.php' );
  }
  
  
  /**
   * Render a partial via a shortcode.
   * 
   * @access public
   * @param Array $atts The shortcode attributes
   * @return String $output The output to display
   */
  public function render_partial_shortcode( $atts ) {
    extract( $atts );
    if ( isset ( $slug ) ) {
      ob_start();
      $this->render_partial( $slug );
      $output = ob_get_contents();
      ob_end_clean();
      return $output;
    }
    return;
  }   
  

  
  
  
  /**
   * Add partials to Theme Customizer.
   * 
   * @access private
   * @return void
   */
  private function add_to_customizer() {
  
    require_once( get_template_directory() . '/classes/class-fl-customizer.php' );
          
    /* Header Panel */
    FLCustomizer::add_panel( 
      'custom-partials', 
      array(
    	  'title'    => _x( 'Custom Partials', 'Customizer panel title.', 'twentysixty-child' ),
        'sections' => $this->get_section_data() 
      ) 
    );    
  } // end function add_to_customizer()
  
  
  /**
   * Gets the data for all sections to include in Theme Customizer.
   * 
   * @access private
   * @return void
   */
  private function get_section_data() {
    
    $section_data = array();
    
    foreach( $this->sections as $section => $title ) {

      $section_data[$section] = array(
  			'title'   => __( $title, 'twentysixty-child' ),
  			'options' => array(
  				$section => array(
      			'setting'   => array(
      				'default'   => 'none',
      				'transport' => 'postMessage'
      			),
      			'control'   => array(
      				'class'         => 'WP_Customize_Control',
      				'label'         => __('Partial File', 'twentysixty-child'),
      				'description'    => 'For an explanation of partials, <a href="' . get_stylesheet_directory_uri() . '/README.html" target="_blank">see the README.</a>',
      				'type'          => 'select',
      				'choices'       => $this->partials
      			)
      		)
        )
      );
    }        
    return $section_data;
  } // end function get_section_data()
  
  
  /*
   * Get a partial content via AJAX.
   * Echoes an HTML response.
   * 
   * @access public
   * @return void
   */
  public function get_partial_ajax() {    
        
    if ( isset ( $_REQUEST['slug'] ) )    
      $this->render_partial( $_REQUEST['slug'] );     
    
    exit;
  }   
  
  
  /**
   * Load the Partials Page Builder Module.
   * 
   * @access public
   * @return void
   */
  public function load_module() {
    if ( class_exists( 'FLBuilder' ) ) {
      FLBuilder::register_module( 'TwentysixtyPartialsModule', array(
          'custom-partials-settings'      => array(
              'title'         => __( 'Settings', 'twentysixty-theme' ),
              'sections'      => array(
                  'select-partial'  => array(
                      'title'         => __( 'Select Partial', 'twentysixty-theme' ),
                      'fields'        => array(
                          'partial'     => array(
                              'type'          => 'select',
                              'label'         => __( 'Partial File', 'twentysixty-theme' ),
                              'options'       => $this->partials   
                          )                            
                      )
                  )
              )
          )
      ) ); 
    } else {
      die('Class no existy');
    }
  } 
}