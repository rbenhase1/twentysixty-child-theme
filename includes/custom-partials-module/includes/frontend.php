<?php
  
  if ( isset( $settings->partial ) && !empty( $settings->partial ) )
    include( get_stylesheet_directory() . '/partials/' . $settings->partial . '.php' );