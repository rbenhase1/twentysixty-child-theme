# Twentysixty Child Theme #

A Wordpress child theme to use in conjunction with the Beaver Builder base theme.

## Setup ##

* Install the Beaver Builder base theme.
* Install the Twentysixty Child Theme (upload to `wp-content/themes`)
* Activate the "2060 Digital Child Theme" under Appearance > Themes.
* Use the Wordpress Theme Customizer to build headers, footers, menus, logos, etc.
* Only edit the theme files when additional/advanced customization is needed


## What Should Go in My Theme? ##

Generally, you should attempt to do as much as you can within the Wordpress Theme Customizer and the Beaver Builder Page Builder (which may be white-labeled as something else, e.g. the "2060 Digital Page Builder"). Adding overrides to your child theme should ONLY be done when absolutely necessary. 

Anything related to the **functionality** of your site is probably better-suited for a site-specific plugin, although you could technically place some light (or theme-related) functionality inside functions-custom.php. Anything related to the **presentation** of your site belongs in the theme. 

## How Should I Edit My Theme? ##

This theme, should you need to make changes to it, is a child theme. It is meant to be edited directly to meet your needs. However, you should take care to edit the right things. The three basic rules are:

* Do NOT put styles in the main style.css file.
* In fact, do NOT edit any CSS files (use SCSS instead).
* Do NOT make changes to functions.php (use functions-custom.php instead).

You also probably do not need to create custom page template files (e.g. to override the files included in the parent theme), because....

### You Can (And Should) Use Custom Partials ###

Custom partials are bits of PHP and HTML that you can include almost anywhere on your site. In this sense, they're like little snippets of custom template files. Partials should be mostly comprised of structured HTML (i.e., what you'd expect a template file to be), rather than super PHP-heavy, as the idea here is to keep your logic separate from your presentation. The gist of it is this:

* Logic (custom functions and fancy PHP that works behind-the-scenes) goes inside functions-custom.php.
* Presentation (HTML markup and front-facing stuff) goes inside your partials folder.

You will find some built-in partials inside the `partials` folder of your theme, but you can also create your own! Any .php file inside this folder will automatically be recognized as a custom partial, with the name (slug) coming from the filename. You can also give your custom partial a nicer, more human-readable name by beginning your file with a PHP comment, like so:

```
<?php 
/*
 * Partial Name: My Partial
 */
?>
```
Once you've created your partial, you can use it in one of three ways:

* Assign it to an certain area of your theme under the "Custom Partials" section of the Theme Customizer (Appearance > Customize in your Wordpress admin).
* Include it inside your posts, pages, and text widgets with a shortcode: `[custom_partial slug="my-partial"]`
* Add it within the Page Builder, using the Custom Partial module (which can be found under "Advanced Modules").

This prevents you from having to create entire template files to override the parent theme, and it gives you  the flexibility of being able to add any code, almost anywhere. Just use caution!

### Other Custom PHP Code and Functions ###
Any custom PHP functions (including filters, actions, or any fancy logic you wrote) should go inside the functions-custom.php file, which is automatically imported by the theme. It is **not** recommended that you make any changes to the main functions.php file, since this loads the theme foundation.

Once again, you probably do not need to use custom page templates with this child theme, since the flexibility of Beaver Builder and the custom partials option above ought to allow for enough customization by themselves.

### Working with SCSS ###

This theme is set up to use SCSS. Since the parent theme will be handling the grid system, and the Theme Customizer will be handling things like logos, colors, and fonts, there should not be a whole lot that you need to do here. Still, the theme styles are broken up into several files:

* **style.scss** - The main stylesheet. You should not edit this file. Rather, you should edit the files it imports, which are as follows:
    * **_base.scss** - Global styles which affect all screen sizes
    * **_768up.scss** - Styles only affecting tablets and desktop-sized screens
    * **_992up.scss** - Styles only affecting desktop-sized screens
    * **_2x.scss** - Styles only affecting retina/high-density displays
* **admin.scss** - The admin stylesheet. This will only be loaded if you enqueue it inside your functions-custom.php file.
* **ie.scss** - Styles specific to Internet Explorer. This will only be loaded if you enqueue it inside your functions-custom.php file.
* **login.scss** - Style specific to the Wordpress login page only. This will only be loaded if you enqueue it inside your functions-custom.php file.

These SCSS files can be found at `library/scss`. They will be compiled (assuming you've set up your project in something which can compile SCSS, like Compass or CodeKit) into CSS files which reside at `library/css`. Except for admin.css and ie.css,these stylesheets will be enqueued automatically by the theme). 

### Images, Javascript, etc. ###

You can add any theme-related images under `library/images`. Javascript can go under `library/js`. Keep in mind that you will still need to enqueue any scripts you place inside this folder within your functions-custom.php file.

## Well, What are you waiting for? ##

Happy theme-building!